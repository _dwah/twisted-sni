## SNI Tester

Adds SNI support to Twisted (13.1) and allows to test against any URL

### Setup

    mkdir venv
    virtualenv ./venv
    ./venv/bin/pip install -r requirements.txt

### Run

Using treq without SNI:

    ./venv/bin/python sni-test.py -l treq https://example.com

Using treq with SNI:

    ./venv/bin/python sni-test.py --sni -l treq https://example.com

Using getPage without SNI:

    ./venv/bin/python sni-test.py -l getPage https://example.com

Using getPage with SNI:

    ./venv/bin/python sni-test.py --sni -l getPage https://example.com
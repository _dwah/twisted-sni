import argparse
from urlparse import urlparse
import treq
from treq.client import HTTPClient
from twisted.internet import reactor
from twisted.internet.defer import inlineCallbacks, returnValue, DeferredList, Deferred
from twisted.internet.ssl import ClientContextFactory
from twisted.internet.task import react
from OpenSSL.SSL import SSL_CB_HANDSHAKE_START, OP_NO_SSLv3
from twisted.web.client import getPage, Agent, HTTPConnectionPool


class SNIClientContextFactory(ClientContextFactory):
    def __init__(self, sni_host_name=None):
        """
        Creates a `ClientContextFactory` that supports SNI and is compatible with both `treq` and `getPage`.

        Args:
            sni_host_name (str|unicode): The host name to connect to. This argument is used for `getPage` calls,
            where the host name is not passed tp `getContext`.

        Returns:
            SNIClientContextFactory: The context factory, intended for one-time use.
        """
        self.sni_host_name = sni_host_name

    def getContext(self, host_name=None, port=None):
        """
        Implements the SPI, compatible with `getPage` and `treq`. `treq` passes in ``host_name`` and ``port``,
        whereas the standard `ClientContextFactory` interface does not take any arguments.

        Args:
            host_name (str|unicode): The host name to connect to. This argument is used in `treq` calls.
            port (int): Unused

        Returns:
            SSL.Context: An SSL Context, configured for SNI and without support for SSLv2 and SSLv3
        """

        def tls_sni_callback(connection, where, _):
            """
            A callback that allows us to set the host name when the handshake starts.

            Args:
                connection (SSL.Connection): The SSL Connection
                where (int): The phase in the handshake process
                _ (int): The return code of the phase (unused)
            """
            if SSL_CB_HANDSHAKE_START == where:
                host = host_name if host_name else self.sni_host_name
                if host is not None:
                    # UTF-8 according to https://www.ietf.org/rfc/rfc3546.txt
                    connection.set_tlsext_host_name(host.encode('utf-8'))

        ctx = ClientContextFactory.getContext(self)
        ctx.set_info_callback(tls_sni_callback)
        # Let's disable SSLv3 if we're already constructing a custom context (POODLE)
        ctx.set_options(OP_NO_SSLv3)

        return ctx


def treq_sni(context_factory):
    agent = Agent(reactor, contextFactory=context_factory, connectTimeout=30)
    return HTTPClient(agent)


@inlineCallbacks
def get_content_treq(url, use_sni):
    client = treq_sni(SNIClientContextFactory()) if use_sni else treq
    resp = yield client.get(url, timeout=30)
    content = yield treq.content(resp)
    returnValue(content)


@inlineCallbacks
def get_content_get_page(url, use_sni):
    netloc = urlparse(url).netloc
    context_factory = SNIClientContextFactory(netloc) if use_sni else None
    content = yield getPage(url, context_factory)
    returnValue(content)


@inlineCallbacks
def get_content(url, lib, sni):
    result = {'url': url}
    try:
        if lib == 'treq':
            result['content'] = yield get_content_treq(url, sni)
        else:
            result['content'] = yield get_content_get_page(url, sni)
        result['error'] = None
        result['error_message'] = None
    except Exception as e:
        result['content'] = ''
        result['error'] = str(type(e))
        result['error_message'] = e.message
    returnValue(result)


def connect(url, lib):
    no_sni = get_content(url, lib, False)
    sni = get_content(url, lib, True)
    return DeferredList([no_sni, sni])


@inlineCallbacks
def main(reactor):
    parser = argparse.ArgumentParser(description='Get a URL and print the output or errors')
    parser.add_argument('-u', '--url', nargs='?', help='the URL to get')
    parser.add_argument('-f', '--file', nargs='?', help='A file with one URL per line')
    parser.add_argument('-l', '--lib', nargs='?', choices=['treq', 'getPage'], default='treq')
    parser.add_argument('-s', '--sni', action='store_true')
    parser.add_argument('-b', '--batch', nargs='?', type=int, default=100, help='Batch size')
    arguments = parser.parse_args()

    if arguments.url:
        content = yield get_content(arguments.url, arguments.lib, arguments.sni)
        print content

    if arguments.file:

        print('Fetching URLs using %s and batch size %d' % (arguments.lib, arguments.batch))

        counts = {
            'count': 0,
            'sni_failure_count': 0,
            'mismatch_count': 0,
            'error_count': 0,
            'ok_count': 0
        }

        error_types = {}
        sni_failures = []

        def print_results():
            print '\x1b[2J\x1b[H'  # clear terminal window
            print '--------------------'
            print 'Count:         %d' % counts['count']
            print 'SNI Failure #: %d' % counts['sni_failure_count']
            print 'Mismatch #:    %d' % counts['mismatch_count']
            print 'Error #:       %d' % counts['error_count']
            print 'OK #:          %d' % counts['ok_count']
            print '--------------------'

            for key, value in error_types.iteritems():
                print '%s %d' % ((key + ':').ljust(30), value)

            print '--------------------'

        def process_results(result_no_sni, result_sni):
            if result_no_sni['error'] is None and result_sni['error'] is not None:
                counts['sni_failure_count'] += 1
                sni_failures.append(result_sni)

            if result_no_sni['error'] != result_sni['error']:
                counts['mismatch_count'] += 1

            counts['count'] += 1
            error = result_sni['error']
            if error is None:
                counts['ok_count'] += 1
            else:
                counts['error_count'] += 1
                error_types[error] = error_types.setdefault(error, 0) + 1

        @inlineCallbacks
        def process_batch(batch):
            results = yield DeferredList(batch)
            for result in results:
                process_results(result[1][0][1], result[1][1][1])

        batch = []
        with open(arguments.file, 'r') as f:
            for url in f:
                url = url.rstrip()

                if url.startswith('http'):

                    deferred = connect(url, arguments.lib)
                    batch.append(deferred)

                    if len(batch) == arguments.batch:
                        yield process_batch(batch)
                        print_results()
                        batch = []

        yield process_batch(batch)
        print_results()

        for sni_failure in sni_failures:
            print 'SNI Failure', sni_failure['url'], sni_failure['error']
            content = yield get_content(sni_failure['url'], arguments.lib, True)
            print 'SNI Failure re-checked:', sni_failure['url'], content['error']


react(main, [])
